package cn.business.main.handler;

import cn.business.bean.vo.ResponseVO;
import cn.business.exception.PayBusinessException;
import cn.business.myenum.HandlerType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Optional;

/**
 * 全局异常处理
 */
@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {
	@ResponseBody
	@ExceptionHandler(value = Exception.class)
	public ResponseVO handle(Exception e) {
		ResponseVO responseVO = new ResponseVO();
		if (e instanceof PayBusinessException) {
			log.error("PayBusinessException异常处理，捕获异常信息:{}", e.getMessage());
			PayBusinessException businessException = (PayBusinessException) e;
			responseVO.setRetCode(businessException.getErrorCode());
			responseVO.setRetMsg(businessException.getErrorMessage());
		} else if (e instanceof MethodArgumentNotValidException) {
			log.error("MethodArgumentNotValidException异常处理，捕获异常信息:{}", e.getMessage());
			MethodArgumentNotValidException methodArgumentNotValidException = (MethodArgumentNotValidException) e;
			BindingResult bindingResult = methodArgumentNotValidException.getBindingResult();
			Optional<String> message = Optional.ofNullable(bindingResult).map(result -> result.getAllErrors()).map(error -> error.get(0).getDefaultMessage());
			responseVO.setRetInfo(HandlerType.SYSTEM_ERROR);
			message.ifPresent(msg -> responseVO.setRetMsg(msg));
		}
//		else if (e instanceof RpcException) {
//			log.error("RpcException异常处理，捕获异常信息:{}", e.getMessage());
//			responseVO.setRetInfo(HandlerType.SYSTEM_ERROR);
//			responseVO.setRetMsg("远程服务繁忙，请稍后再试");
//		}
		else {
			log.error("Exception异常处理，捕获异常信息:{}", e.getMessage());
			responseVO.setRetInfo(HandlerType.SYSTEM_ERROR);
			responseVO.setRetMsg(e.getMessage());
		}

		return responseVO;
	}
}
