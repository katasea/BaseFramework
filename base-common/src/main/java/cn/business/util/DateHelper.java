package cn.business.util;

import org.apache.commons.lang.time.DateUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class DateHelper {
	public static String FORMAT_FULL_DATE = "yyyy-MM-dd HH:mm:ss";
	public static String FORMAT_PLAIN_DATE = "yyyyMMdd";

	private DateHelper() {
	}

	public static DateHelper instance = new DateHelper();

	static {
		TimeZone time = TimeZone.getTimeZone("GMT+8");
		// 设置为东八区
		time = TimeZone.getDefault();
		// 这个是国际化所用的
		TimeZone.setDefault(time);
	}

	public static Date getCurrentDate() {
		return Calendar.getInstance().getTime();
	}

	public static String getCurrentDateStr(String pattern) {
		return GeneralHelper.date2Str(getCurrentDate(), pattern);
	}

	/**
	 * 获取上个月日期
	 *
	 * @param pattern
	 * @return
	 */
	public static String getLastMonthDate(String pattern) {
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_MONTH, -30);
		date = calendar.getTime();
		return GeneralHelper.date2Str(date, pattern);
	}

	/**
	 * 转换字符串类型的日期 的格式,
	 *
	 * @param dateStr
	 * @param fromPattern 原始 格式
	 * @param toPattern   要转换的格式
	 * @return
	 */
	public static String changeDatePattern(String dateStr, String fromPattern,
	                                       String toPattern) {
		SimpleDateFormat sdf = new SimpleDateFormat(fromPattern);
		Date d = null;
		try {
			d = sdf.parse(dateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return GeneralHelper.date2Str(d, toPattern);
	}

	/**
	 * 将日期格式如20140101转换成带-的日期格式
	 *
	 * @param dateStr
	 * @return
	 */
	public static String convertToStrDate(String dateStr) {
		String retDate = null;
		if (dateStr == null)
			return retDate;
		retDate = dateStr.replaceAll("0{6}", "");
		if (retDate.equals(dateStr)) {
			retDate = dateStr.replaceAll("0{2}", "");
		}
		try {
			if (retDate.length() == 14) {
				retDate = retDate.replaceAll(
						"(\\d{4})(\\d{2})(\\d{2})(\\d{2})(\\d{2})(\\d{2})",
						"$1-$2-$3 $4:$5:$6");
			} else if (retDate.length() == 8) {
				retDate = retDate.replaceAll("(\\d{4})(\\d{2})(\\d{2})",
						"$1-$2-$3");
			} else {
				retDate = retDate.replaceAll(
						"(\\d{4})(\\d{2})(\\d{2})(\\d{2})(\\d{2})",
						"$1-$2-$3 $4:$5");
			}
		} catch (Exception e) {
			retDate = dateStr;
		}
		return retDate;
	}

	/**
	 * 获取上一天
	 *
	 * @param pattern
	 * @return
	 */
	public static String getLastDateStr(String pattern) {
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_MONTH, -1);
		date = calendar.getTime();
		return GeneralHelper.date2Str(date, pattern);
	}

	/**
	 * 获取上一天
	 *
	 * @param pattern
	 * @return
	 */
	public static String getLastDateStr(String pattern, int day) {
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_MONTH, day);
		date = calendar.getTime();
		return GeneralHelper.date2Str(date, pattern);
	}

	/**
	 * 获取指定日期上一天 传入与传出格式
	 *
	 * @param beginDate
	 * @param pattern   传入与传出日期格式 同一个
	 * @return
	 */
	public static String getLastDateStr(String beginDate, String pattern) {
		Date date = new Date();
		DateFormat format = new SimpleDateFormat(pattern);
		try {
			date = format.parse(beginDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_MONTH, -1);
		date = calendar.getTime();
		return GeneralHelper.date2Str(date, pattern);
	}

	/**
	 * 返回一段日期的每个值的集合;(如:20150802到20151010这个日期段的每一天,包含起止日期)
	 *
	 * @param beginDate
	 * @param endDate
	 * @param pattern
	 * @return
	 */
	public static List<String> fromBeginToEndDate(String beginDate,
	                                              String endDate, String pattern) throws ParseException {

		List<String> dates = new ArrayList<String>();
		Date d1 = new SimpleDateFormat(pattern).parse(beginDate);// 定义起始日期
		Date d2 = new SimpleDateFormat(pattern).parse(endDate);// 定义结束日期
		Calendar c = Calendar.getInstance();// 定义日期实例
		c.setTime(d1);// 设置日期起始时间
		while (c.getTime().before(d2)) {// 判断是否到结束日期
			SimpleDateFormat sdf = new SimpleDateFormat(pattern);
			String str = sdf.format(c.getTime());
			c.add(Calendar.DAY_OF_MONTH, 1);// 进行当前日期天数加1
			dates.add(str);
		}
		String lastDay = new SimpleDateFormat(pattern).format(c.getTime());
		dates.add(lastDay);
		return dates;
	}

	// 上周一
	public static String getLastWeekMonday(String dateStr, String in, String out) {
		Date date = GeneralHelper.str2Date(dateStr, in);
		return GeneralHelper.date2Str(getLastWeekMonday(date), out);
	}

	// 上周一
	public static Date getLastWeekMonday(Date date) {
		Date a = DateUtils.addDays(date, -1);
		Calendar cal = Calendar.getInstance();
		cal.setTime(a);
		cal.add(Calendar.WEEK_OF_YEAR, -1);// 一周
		cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

		return cal.getTime();
	}

	// 上周一
	public static String getLastWeekSunday(String dateStr, String in, String out) {
		Date date = GeneralHelper.str2Date(dateStr, in);
		return GeneralHelper.date2Str(getLastWeekSunday(date), out);
	}

	// 获取上周日
	public static Date getLastWeekSunday(Date date) {
		Date a = DateUtils.addDays(date, -1);
		Calendar cal = Calendar.getInstance();
		cal.setTime(a);
		cal.set(Calendar.DAY_OF_WEEK, 1);

		return cal.getTime();
	}

	public static boolean valiad(String dateStr, String format) {
		if (CommonUtil.isEmpty(dateStr)) {
			return true;
		}

		SimpleDateFormat dateFormat = new SimpleDateFormat(format);

		try {
			Date date = dateFormat.parse(dateStr);

			if (!dateStr.equals(dateFormat.format(date))) {
				return false;
			}
		} catch (ParseException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	public static int compareDate(String DATE1, String DATE2) {
		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		try {
			Date dt1 = df.parse(DATE1);
			Date dt2 = df.parse(DATE2);
			if (dt1.getTime() > dt2.getTime()) {
				// System.out.println("dt1 在dt2前");
				return 1;
			} else if (dt1.getTime() < dt2.getTime()) {
				// System.out.println("dt1在dt2后");
				return -1;
			} else {
				return 0;
			}
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return 0;
	}
}
