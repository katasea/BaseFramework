package cn.business.util;

import cn.business.bean.vo.RequestVO;
import cn.business.exception.PayBusinessException;
import cn.business.myenum.HandlerType;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.*;

/**
 * 自定义参数校验工具类
 */
public class ParamValid {
	private static final Logger LOG = LoggerFactory.getLogger(ParamValid.class);
	private static final ObjectMapper MAPPER = new ObjectMapper();

	public static Map<String, Map<String, ParamCheck>> miParamMap = new HashMap<>();
	private static Map<String, Map<String, ParamCheck>> notNullParamsMap = new HashMap<>();
	private static Map<String, Map<String, ParamCheck>> canNullParamsMap = new HashMap<>();
	public static final String configPath = "/config/params-valiad-config.json";

	public static void valiadParam(RequestVO request) throws PayBusinessException {
		JSONObject params = (JSONObject) request.getBizObj();

		// 1 非空校验配置
		Map<String, ParamCheck> funcNotNullMap = getFuncNotNullMap(request.getMethod());

		for (Map.Entry<String, ParamCheck> entry : funcNotNullMap.entrySet()) {
			ParamCheck check = entry.getValue();

			// // 校验证件号
			if (check.getValidType().startsWith(ValidType.idno)) {
				String id_type = params.getString("idType");
				String value = params.getString(entry.getKey());
				// 仅校验身份证A
				if ("0".equals(id_type)) {
					if (!StringUtils.isEmpty(value)) {
						String valid = IDCard.IDCardValidate(value);
						if (!StringUtils.isEmpty(valid)) {
							LOG.error("参数错误{},{},{}", entry.getKey(), value, valid);
							throw new PayBusinessException(HandlerType.REQ_POST_TEXT_ILLEGAL.getRetCode(), "参数" + check.getName() + "(" + entry.getKey() + ")" + valid);
						}
					}
				}
			}

			if (params.get(entry.getKey()) == null || params.get(entry.getKey()).toString().trim().equals("")) {
				throw new PayBusinessException(HandlerType.REQ_POST_TEXT_ILLEGAL.getRetCode(), "参数" + check.getName() + "(" + entry.getKey() + ")不允许为空");
			} else {
				if (entry.getValue() != null)
					valiadParam(entry.getKey(), params.get(entry.getKey()), entry.getValue());
			}
		}

		// 2 可空校验配置
		Map<String, ParamCheck> funcCanNullMap = getFuncCanNullMap(request.getMethod());

		// 剔除已校验的非空配置
		Set<String> keys = new HashSet<>(params.keySet());
		keys.removeAll(funcNotNullMap.keySet()); // 取差集

		for (String key : keys) {
			if (funcCanNullMap.get(key) != null) {
				valiadParam(key, params.get(key), funcCanNullMap.get(key));
			}
		}
	}

	/**
	 * 校验参数方法
	 *
	 * @param key
	 * @param valObj
	 * @param check
	 */
	private static void valiadParam(String key, Object valObj, ParamCheck check) throws PayBusinessException {
		// 字符串
		String value = null;
		if (valObj instanceof String) {
			value = CommonUtil.nullToStr(String.valueOf(valObj));
		} else if (valObj instanceof Integer) {
			value = CommonUtil.nullToZero(String.valueOf(valObj));
		}

		// 校验长度
		if (check.getMaxLength() > 0 && value.length() > check.getMaxLength()) {
			LOG.error("参数错误{},{}", key, value);
			throw new PayBusinessException(HandlerType.REQ_POST_TEXT_ILLEGAL.getRetCode(), "参数" + check.getName() + "(" + key + ")允许最大长度" + check.getMaxLength() + "字符");
		}

		if (check.getValidType() != null) {
			validType(key, value, check);
		}

		// 校验参数范围
		String[] range = check.getRange();

		if (range != null && range.length > 0) {
			List<String> lrange = Arrays.asList(range);

			if (check.isRequired()) {
				if (!lrange.contains(value)) {
					throw new PayBusinessException(HandlerType.REQ_POST_TEXT_ILLEGAL.getRetCode(), "参数" + check.getName() + "(" + key + ")值无效,参数范围" + Arrays.toString(range));
				}
			} else {
				if (StringUtils.isNotEmpty(value)) {
					if (!lrange.contains(value)) {
						throw new PayBusinessException(HandlerType.REQ_POST_TEXT_ILLEGAL.getRetCode(), "参数" + check.getName() + "(" + key + ")值无效,参数范围" + Arrays.toString(range));
					}
				}
			}
		}
//		}
	}

	private static void validType(String key, String value, ParamCheck check) throws PayBusinessException {
		// 校验整数
		if (ValidType.number.equals(check.getValidType())) {
			if (!StringUtils.isEmpty(value) && !ValidUtils.isNumber(value)) {
				LOG.error("参数错误{},{}", key, value);
				throw new PayBusinessException(HandlerType.REQ_POST_TEXT_ILLEGAL.getRetCode(), "参数" + check.getName() + "(" + key + ")需为整数值类型");
			}
		}

		// 校验金额
		if (check.getValidType().indexOf(ValidType.amount) > -1) {
			if (!StringUtils.isEmpty(value)) {
				if (!ValidUtils.isAbsAmount(value)) { // 正数金额
					LOG.error("参数错误{},{}", key, value);
					throw new PayBusinessException(HandlerType.REQ_POST_TEXT_ILLEGAL.getRetCode(), "金额类参数" + check.getName() + "(" + key + ")格式不合法,单位为元，需为正数");
				}
			}
		}

		// 校验身份证
		// if (check.getValidType().indexOf(ValidType.idno) > -1) {
		// if (!StringUtils.isEmpty(value)) {
		// String valid = IDCard.IDCardValidate(value);
		//
		// if (!StringUtils.isEmpty(valid)) {
		// LOG.error("参数错误{},{},{}", key, value, valid);
		// throw new AppBusinessException(TransRet.RETCODE_REQUEST_PARAMS_ERROR,
		// "参数" + check.getName() + "(" + check.getKey() + ")" + valid);
		// }
		// }
		// }

		// 校验日期
		if (check.getValidType().indexOf(ValidType.datetime) > -1) {
			String formatDate = "yyyyMMddHHmmss"; // 默认日期格式

			try {
				String type = check.getValidType();

				if (StringUtils.isNotEmpty(type) && type.indexOf('(') > 0 && type.lastIndexOf(")") > 0) {
					String format = type.substring(type.indexOf('(') + 1, type.lastIndexOf(")"));

					if (StringUtils.isNotEmpty(format))
						formatDate = format;
				}

				if (!DateHelper.valiad(value, formatDate)) {
					LOG.error("参数错误{},{},{}", key, value, formatDate);
					throw new PayBusinessException(HandlerType.REQ_POST_TEXT_ILLEGAL.getRetCode(), "参数" + check.getName() + "(" + key + ")" + "日期不合法," + formatDate);
				}
			} catch (PayBusinessException me) {
				throw me;
			} catch (Exception e) {
				LOG.error("参数异常{},{},{}", key, value, formatDate);
				throw new PayBusinessException(HandlerType.REQ_POST_TEXT_ILLEGAL.getRetCode(), "应用金额日期不合法");
			}
		}

	}

	/**
	 * 加载参数配置
	 */
	static {
		InputStream miparam = null;

		try {
			String paramsValiadConfigPath = YamlUtil.getClassResources() + configPath; // yaml路径
			LOG.info("加载参数校验配置文件路径：{}", paramsValiadConfigPath);
			File jsonFile = new File(paramsValiadConfigPath);
			miparam = new FileInputStream(jsonFile);
			// 加载入参配置
			miParamMap = MAPPER.readValue(miparam, new TypeReference<Map<String, Map<String, ParamCheck>>>() {
			});

			for (Map.Entry<String, Map<String, ParamCheck>> entry : miParamMap.entrySet()) {
				Map<String, ParamCheck> params = entry.getValue();

				for (Map.Entry<String, ParamCheck> param : params.entrySet()) {
					Map<String, ParamCheck> notNullChecks = notNullParamsMap.get(entry.getKey());

					if (notNullChecks == null)
						notNullChecks = new HashMap<>();

					Map<String, ParamCheck> canNullChecks = canNullParamsMap.get(entry.getKey());

					if (canNullChecks == null)
						canNullChecks = new HashMap<>();

					if (param.getValue().isRequired()) {
						notNullChecks.put(param.getKey(), param.getValue());
						notNullParamsMap.put(entry.getKey(), notNullChecks);

					} else {
						canNullChecks.put(param.getKey(), param.getValue());
						canNullParamsMap.put(entry.getKey(), canNullChecks);
					}
				}

			}

			if (miParamMap != null)
				LOG.info("加载参数配置,{}", miParamMap.entrySet());

			LOG.debug("非空参数配置,{}", MAPPER.writeValueAsString(notNullParamsMap));
			LOG.debug("可空参数配置,{}", MAPPER.writeValueAsString(canNullParamsMap));
		} catch (Exception e) {
			LOG.error("参数配置加载失败", e);
			e.printStackTrace();
		}
	}

	/**
	 * 接口参数-可空参数
	 *
	 * @param func
	 * @return
	 */
	private static Map<String, ParamCheck> getFuncCanNullMap(String func) {
		Map<String, ParamCheck> funcMap = canNullParamsMap.get(func);
		Map<String, ParamCheck> getMap = new HashMap<>();
		if (funcMap != null && funcMap.size() > 0)
			getMap.putAll(funcMap);
		return getMap;
	}

	/**
	 * 接口参数-非空参数
	 *
	 * @param func
	 * @return
	 */
	private static Map<String, ParamCheck> getFuncNotNullMap(String func) {
		Map<String, ParamCheck> funcMap = notNullParamsMap.get(func);
		Map<String, ParamCheck> getMap = new HashMap<>();
		if (funcMap != null && funcMap.size() > 0)
			getMap.putAll(funcMap);
		return getMap;
	}

}
