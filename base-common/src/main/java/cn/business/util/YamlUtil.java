package cn.business.util;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * type.yml 配置文件对应下拉集合显示
 */
@Slf4j
public class YamlUtil {
	public static final String RFL_SRV_CONFIG = "/config/reflection-service-config.yml";
	public static final String DEFAULT_REL_BEAN_CONFIG = "open-platform";
	public static final String RFL_BEAN_CONFIG = "/config/reflection-service-bean-config.yml";

	/**
	 * 读取下拉状态配置参数返回map
	 * @param typeName 检索 a/b/c
	 */
	public static Map getTypePropertieMap(String config, String typeName) throws IOException{
		String yamlPath = getClassResources()+ config; // yaml路径
		log.info("查找配置文件信息，关键字：{}，检索文件路径：{}",typeName,yamlPath);
		File yamlFile = new File(yamlPath);
		Yaml yaml = new Yaml();
		Map map = yaml.loadAs(new FileInputStream(yamlFile), Map.class);
		//检索
		String[] words = typeName.split("/");
		for(String word : words) {
			if(map!=null && map.keySet().size()>0) {
				map = (HashMap) map.get(word);
			}else {
				log.error("检索配置文件失败了：{},{}",typeName,word);
			}
		}
		log.info("获取配置文件内容为：{}",map);
		return map;
	}
	/**
	 * Yaml转json
	 * @param config
	 * @return
	 */

	@SuppressWarnings("unchecked")
	public static JSONObject convertToJson(String config) throws FileNotFoundException {
		String yamlPath = getClassResources()+ config; // yaml路径
		log.info("查找配置文件信息，检索文件路径：{}",yamlPath);
		File yamlFile = new File(yamlPath);
		Yaml yaml = new Yaml();
		Map map = yaml.loadAs(new FileInputStream(yamlFile), Map.class);
		JSONObject jsonObject=new JSONObject(map);
		return jsonObject;
	}

	/**
	 * Yaml转Map
	 * @param config
	 * @return
	 */

	@SuppressWarnings("unchecked")
	public static Map convertToMap(String config) throws FileNotFoundException {
		String yamlPath = getClassResources()+ config; // yaml路径
		log.info("查找配置文件信息，检索文件路径：{}",yamlPath);
		File yamlFile = new File(yamlPath);
		Yaml yaml = new Yaml();
		Map map = yaml.loadAs(new FileInputStream(yamlFile), Map.class);
		return map;
	}



	/**
	 * Yaml转List
	 * @param config
	 * @return
	 */

	@SuppressWarnings("unchecked")
	public static List<String> toList(String config) throws FileNotFoundException {
		String yamlPath = getClassResources()+ config; // yaml路径
		log.info("查找配置文件信息，检索文件路径：{}",yamlPath);
		File yamlFile = new File(yamlPath);
		Yaml yaml = new Yaml();
		List list = yaml.loadAs(new FileInputStream(yamlFile), List.class);
		return list;
	}
	/*
	 * 获取classpath2
	 */
	public static String getClassResources(){
		String path =  System.getProperty("user.dir");
		if(path.indexOf(":") != 1){
			path = File.separator + path;
		}
		return path;
	}

}
