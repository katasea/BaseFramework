package cn.business.util;

import lombok.Data;

/**
 * 参数校验类
 */
@Data
public class ParamCheck {
	String name;
	boolean required;
	int maxLength;
	String validType;
	String[] range;

	public ParamCheck() {
	}

	public ParamCheck(int maxLength, String validType) {
		super();
		this.maxLength = maxLength;
		this.validType = validType;
	}

	public ParamCheck(int maxLength, String validType, String... range) {
		super();
		this.maxLength = maxLength;
		this.validType = validType;
		this.range = range;
	}

}
