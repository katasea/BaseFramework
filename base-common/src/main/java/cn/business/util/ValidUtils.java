package cn.business.util;

public class ValidUtils {

	public static boolean isNumber(String val) {
		return val.matches("[\\d]+");
	}

	public static boolean isAbsAmount(String val) {
		return val.matches("^(([1-9]\\d{0,9})|0)(\\.\\d{1,2})?$");
	}
}
