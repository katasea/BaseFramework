package cn.business.util;

public class ValidType {
	public static final String string = "string";
	public static final String number = "number";
	public static final String amount = "amount";
	public static final String json = "json";
	public static final String idno = "idno";
	public static final String datetime = "datetime";
	public static final String mimode = "mimode";
	public static final String notnull = "notnull";
}
